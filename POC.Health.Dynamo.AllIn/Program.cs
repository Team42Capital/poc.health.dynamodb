﻿// See https://aka.ms/new-console-template for more information

using System.Collections.Concurrent;
using Microsoft.Extensions.DependencyInjection;
using System.Reactive.Linq;
using Common.Infrastructure.BackgroundProcessor.Services;
using POC.Health.Dynamo;
using POC.Health.Dynamo.AllIn;
using Serilog;

Log.Logger = new LoggerConfiguration()
    .WriteTo.Async(c=>c.Console())
    .WriteTo.Async(c=>c.File("poc.dynamo.log"))
    .MinimumLevel.Information()
    .CreateLogger();

var services = new ServiceCollection();
hgInit.ConfigureServices(services);
var serviceProvider = services.BuildServiceProvider();
var healthService = serviceProvider.GetService<AwsDynamoProcessorHealthService>();



var nServices = 50;
var nClientsPerService = 100;
var numberOfThreads = 16;
var serviceObservables = new ConcurrentBag<IObservable<long>>();
var clientObservables = new ConcurrentBag<IObservable<long>>();

var scheduler = new hgPriorityScheduler();
var cancellationToken = new CancellationTokenSource();

var threads = Enumerable.Range(0, numberOfThreads).Select( i =>
    Task.Run( () => scheduler.WorkerProc(cancellationToken.Token) )
).ToList();

for (var iService = 0; iService < nServices; ++iService)
{
    var iS = iService;

    Task.Run(() =>
    {
        var service = new hgPocService(healthService, iS, scheduler);
        var pulse = service.Run();
        serviceObservables.Add(pulse);
    });

    for (var iClient = 0; iClient < nClientsPerService; ++iClient)
    {
        var iC = iClient;
        Task.Run(() =>
        {
            var client = new hgPocClient(healthService, iS, iC, scheduler);
            var pulseC = client.Run();
            clientObservables.Add(pulseC);

        });
    }
}

var clients = Observable.Merge(clientObservables);
var clientsMinutes = clients.Window(TimeSpan.FromMinutes(1));
var clientMinutesCount = clientsMinutes.Select(x => x.Count());
clientMinutesCount.Subscribe(x =>
{
    Task.Run(() =>
    {
        var latest = x.Latest().ToList();
        var nLatest = (latest.Any()) ?latest.Last() : 0.0;
        Log.Information($"Average number of requests per second over the last minute: {nLatest / 60.0}");

    });
});

var logPulse = Observable
    .Interval(TimeSpan.FromSeconds(5));
logPulse.Subscribe(x =>
{
    Task.Run(() =>
    {
        Log.Information(
            $"Pending Updates {AwsDynamoProcessorHealthService.UpdateRunning.Count} |  Checks = {AwsDynamoProcessorHealthService.CheckRunning.Values.Sum()} | Tasks: {scheduler.QueueSize} ");
    });
});

clients.Wait();