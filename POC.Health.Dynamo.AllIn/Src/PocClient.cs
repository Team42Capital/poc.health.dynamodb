﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Core.BackgroundProcessor.Models.Cached;
using Common.Core.BackgroundProcessor.Services;
using MathNet.Numerics.Distributions;
using POC.Health.Dynamo.AllIn;
using Serilog;

namespace POC.Health.Dynamo
{
    public class hgPocClient
    {
        #region Properties
        public TimeSpan HealthSpan { get; set; } = TimeSpan.FromSeconds(1);
        public IProcessorHealthService HealthService { get; private set; }

        public int ServiceId { get; private set; }
        public int ClientId { get; private set; }

        public hgPriorityScheduler PriorityScheduler { get; private set; }
        #endregion

        #region Fields

        private ProcessorHealth m_health;


        #endregion
        public hgPocClient(IProcessorHealthService healthService, int serviceId, int clientId, hgPriorityScheduler scheduler=null)
        {
            this.ServiceId = serviceId;
            this.ClientId = clientId;
            this.HealthService = healthService;
            m_health = new ProcessorHealth { ProcessorId = $"POC Service {this.ServiceId}" };
            this.PriorityScheduler = scheduler;
        }
        public IObservable<long> Run()
        {

            Log.Debug($"Starting client {this.ServiceId}/{this.ClientId}");

            var pulse = Observable
                .Interval(this.HealthSpan);
            pulse
                .SubscribeOn(Scheduler.Default)
                .Subscribe(async x =>
                {
                    bool ok = false;
                    if (this.PriorityScheduler is null)
                    {
                        ok = await this.HealthService.IsHealthyAsync(m_health);
                        if (!ok)
                            Log.Debug($"Service {this.ServiceId} was not healthy (client {this.ClientId})");
                        
                    }
                    else
                    {
                        this.PriorityScheduler.EnqueueTask(async () =>
                        {
                            ok = await this.HealthService.IsHealthyAsync(m_health);
                            if (!ok)
                                Log.Debug($"Service {this.ServiceId} was not healthy (client {this.ClientId})");
                        },100);
                    }
                });
            return pulse;
        }
    }
}
