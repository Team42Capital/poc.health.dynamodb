﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Common.Core.BackgroundProcessor.Configuration.Models;
using Common.Infrastructure.BackgroundProcessor.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace POC.Health.Dynamo
{
    public static class hgInit
    {
        public static async void CheckTable(AmazonDynamoDBClient client, string tableName)
        {
            var response = await client.ListTablesAsync();
            var ok = response.TableNames.Contains(tableName);

            if (!ok)
            {
                var request = new CreateTableRequest
                {
                    TableName = tableName,
                    AttributeDefinitions = new List<AttributeDefinition>
                    {
                        new AttributeDefinition
                        {
                            AttributeName = "Heartbeat",
                            AttributeType = "S"
                        }
                    },
                    KeySchema = new List<KeySchemaElement>
                    {
                        new KeySchemaElement
                        {
                            AttributeName = "Service",
                            KeyType = "HASH"  //Partition key
                        }
                    },
                };

                var responseCreate = await client.CreateTableAsync(request);
            }
        }

        public static void ConfigureServices(IServiceCollection services)
        {

            // build config
            services.Configure<AwsDynamoConfiguration>(x=>{});
            services.AddTransient<AwsDynamoProcessorHealthService>();
        }
    }
}
