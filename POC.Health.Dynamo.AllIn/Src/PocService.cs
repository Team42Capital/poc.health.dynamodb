﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Core.BackgroundProcessor.Models.Cached;
using Common.Core.BackgroundProcessor.Services;
using MathNet.Numerics.Distributions;
using POC.Health.Dynamo.AllIn;
using Serilog;

namespace POC.Health.Dynamo
{
    public class hgPocService
    {
        #region Properties
        public TimeSpan HealthSpan { get; set; } = TimeSpan.FromSeconds(20);
        public TimeSpan ExpectedLifeSpan { get; set; } = TimeSpan.FromSeconds(600);

        public TimeSpan DeathSpan { get; set; } = TimeSpan.FromSeconds(60);

        public IProcessorHealthService HealthService { get; private set; }

        public int Id { get; private set; }
        
        public hgPriorityScheduler PriorityScheduler { get; private set; }


        #endregion

        #region Fields

        private ProcessorHealth m_health;


        #endregion
        public hgPocService(IProcessorHealthService healthService, int id = -1, hgPriorityScheduler scheduler = null)
        {
            this.Id = (id < 0) ? new Random().Next() : id;
            this.HealthService = healthService;
            m_health = new ProcessorHealth { ProcessorId = $"POC Service {this.Id}" };
            this.PriorityScheduler = scheduler;
        }
        public IObservable<long> Run()
        {
            var expo = new Exponential(1.0 / ExpectedLifeSpan.TotalSeconds);
            var lifeTime = TimeSpan.FromSeconds(expo.Sample());

            Log.Information($"Starting service {this.Id} for {lifeTime}");

            var pulse = Observable
                .Interval(this.HealthSpan)
                .TakeUntil(Observable.Timer(lifeTime));
            pulse = Observable.Return((long)0).Concat(pulse);
            pulse
                .SubscribeOn(Scheduler.Default)
                .Subscribe(async x =>
                {
                    if (this.PriorityScheduler == null)
                        await this.HealthService.UpdateHealthAsync(m_health);
                    else 
                        this.PriorityScheduler.EnqueueTask(()=>this.HealthService.UpdateHealthAsync(m_health),1);
                });
            var pulse2 = pulse.Finally(() => Log.Information($"Stopping service {this.Id} for {this.DeathSpan}"));
            var pulse3 = pulse2.Concat(Observable.Timer(this.DeathSpan))
                .Finally(() => Log.Information($"Restarting service {this.Id} for {lifeTime}"));

            var pulse4 = Observable.Repeat(pulse3);

            return pulse4;

        }
    }
}
