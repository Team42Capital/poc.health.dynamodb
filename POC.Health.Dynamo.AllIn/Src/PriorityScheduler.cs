using System.Collections.Concurrent;
using Serilog;

namespace POC.Health.Dynamo.AllIn;

public class hgPriorityScheduler
{
    #region Properties

    public int QueueSize => m_taskQueue.Count;

    #endregion

    #region Fields

    private object m_locker = new object();

    private PriorityQueue<Func<Task>, int> m_taskQueue = new PriorityQueue<Func<Task>, int>(); 
    #endregion

    #region Procedures

    public void EnqueueTask(Func<Task> task, int priority)
    {
        lock (m_locker)
        {
            m_taskQueue.Enqueue(task, priority);
        }
        
    }
    
    
    public async Task WorkerProc(CancellationToken token)
    {
        while (!token.IsCancellationRequested)
        {
            bool ok = false;
            Func<Task> action = null;
            lock (m_locker)
            {
                ok = m_taskQueue.TryDequeue(out action, out var priority);
            }
            if (ok)
            {
                await action();
                continue;
            }
            
            await Task.Yield();
        }
    }


    #endregion
}