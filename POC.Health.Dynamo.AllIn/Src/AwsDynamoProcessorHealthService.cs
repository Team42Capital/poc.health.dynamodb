﻿using Common.Core.BackgroundProcessor.Models.Cached;
using Common.Core.BackgroundProcessor.Services;
using Common.Core.Redis.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using Amazon.DynamoDBv2.Model;
using Common.Core.BackgroundProcessor.Configuration.Models;
using POC.Health.Dynamo;
using Serilog;

namespace Common.Infrastructure.BackgroundProcessor.Services
{
    public class AwsDynamoProcessorHealthService : IProcessorHealthService
    {
        #region Fields
        private readonly AwsDynamoConfiguration m_configOptions;

        #endregion
        #region Properties
        public TimeSpan Expiry { get; } = TimeSpan.FromMinutes(2);
        public TimeSpan UpdateInterval { get; } = TimeSpan.FromSeconds(20);

        public static ConcurrentDictionary<string, DateTime> UpdateRunning { get; } =
            new ConcurrentDictionary<string, DateTime>();
        public static ConcurrentDictionary<string, int> CheckRunning { get; } =
            new ConcurrentDictionary<string, int>();



        #endregion
        public bool IsHealthy(ProcessorHealth health)
        {
            throw new NotImplementedException();
        }

        public void UpdateHealth(ProcessorHealth health)
        {
            throw new NotImplementedException();
        }
        public AwsDynamoProcessorHealthService(IOptionsMonitor<AwsDynamoConfiguration> dynamoOptions)
        {
            m_configOptions = dynamoOptions.Get("HealthAwsDynamoOptions");
        }

        private object m_locker = new object();
        public async Task<bool> IsHealthyAsync(ProcessorHealth health)
        {
            Log.Debug($"IsHealthy {health.Key}");

            lock (m_locker)
            {
                if (!CheckRunning.ContainsKey(health.ProcessorId))
                    CheckRunning[health.ProcessorId] = 1;
                else
                    CheckRunning[health.ProcessorId] = CheckRunning[health.ProcessorId] + 1;
               
            }

            var sw = new Stopwatch();
            sw.Start();

            try
            {
                var expiryDate = DateTime.UtcNow.AddSeconds(-Expiry.TotalSeconds);
                var client = hgDynamoDB.GetClient();
                string tableName = "HG.POC.Lock";

                var request = new GetItemRequest
                {
                    TableName = tableName,
                    Key = new Dictionary<string, AttributeValue>()
                        { { "Service", new AttributeValue { S = health.Key } } },
                };
                var response = await client.GetItemAsync(request);

                var isHealthy = false;
                DateTime timeStamp = new DateTime(1974,1,18);
                if (response.IsItemSet)
                {
                    var sTimeStamp = response.Item["Timestamp"].S;
                    timeStamp = DateTime.Parse(sTimeStamp, null, DateTimeStyles.AssumeUniversal);
                    isHealthy = timeStamp >= expiryDate;
                    if (!isHealthy)
                        Log.Debug($"Not Healthy: last timestamp {timeStamp} | expiry date {expiryDate}");

                }
                else
                {
                    Log.Warning("Item not set");
                }

                var elapsed = sw.Elapsed;
                if (elapsed.TotalSeconds > 1)
                    Log.Error($"IsHealthy took more than 1 sec! ({elapsed.TotalSeconds} sec)");

                
                return isHealthy;

            }
            catch (Exception ex)
            {

                Log.Error($"IsHealthy failed ({health.Key}) (after {sw.Elapsed.TotalSeconds} secs): {ex.ToString()}");
                return false;
            }
            finally
            {
                lock (m_locker)
                {
                    CheckRunning[health.ProcessorId] -= 1;    
                }
                Log.Debug($"IsHealthy took {sw.Elapsed}");
            }
        }

        public async Task UpdateHealthAsync(ProcessorHealth health)
        {
            UpdateRunning[health.ProcessorId] = DateTime.UtcNow;
            Log.Debug($"UpdateHealth {health.Key}");
            var now = DateTime.UtcNow.ToString("o");
            var sw = new Stopwatch();
            sw.Start();
            var expiryDate = DateTime.UtcNow.AddSeconds(-Expiry.TotalSeconds);
            var client = hgDynamoDB.GetClient();
            string tableName = "HG.POC.Lock";

            var request = new UpdateItemRequest
            {
                TableName = tableName,
                Key = new Dictionary<string, AttributeValue>() { { "Service", new AttributeValue { S = health.Key } } },
                ExpressionAttributeNames = new Dictionary<string, string>()
                {
                    {"#T", "Timestamp"}
                },
                ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
                {
                    {":t",new AttributeValue { S = now}}
                },

                UpdateExpression = "SET #T = :t"
            };
            var response = await client.UpdateItemAsync(request);

            var elapsed = sw.Elapsed;
            if (elapsed.TotalSeconds > 1)
                Log.Error($"UpdateHealth took more than 1 sec! ({elapsed.TotalSeconds} sec)");

            UpdateRunning.Remove(health.ProcessorId, out var toto);
        }
    }
}